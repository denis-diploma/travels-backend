import { Controller, Get, Post, Body, Res, Req, Param, HttpService, Query } from '@nestjs/common';
import { UsersService } from '../users';
import { ConfigService } from 'nestjs-config';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';
import {verifyToken} from './authenticate.helpers';
import {resolve} from 'path';
import {unlinkSync} from 'fs';

@Controller('authenticate')
export class AuthenticateController {
  constructor(
    private readonly config: ConfigService,
    private readonly userService: UsersService,
    private readonly http: HttpService,
  ) {
  }

  @Post('login')
  async login(
    @Body('email') email: string,
    @Body('password') password: string,
    @Res() res,
  ) {
    if (!email || !password) {
      return res.status(400).send({
        error: true,
        message: 'Credentials is invalid',
      });
    }

    const user = await this.userService.getUserByEmail(email);

    if (!user) {
      return res.status(404).send({
        error: true,
        message: 'User not found',
      });
    }

    const passwordIsCorrect = await bcrypt.compare(password, user.password);

    if (!passwordIsCorrect) {
      return res.status(401).send({
        error: true,
        message: 'Password is incorrect',
      });
    }

    const payload = {
      userId: user.userId,
    };

    const accessToken = this.createToken(this.config.get('authenticate.accessTokenExpires'), payload);
    const refreshToken = this.createToken(this.config.get('authenticate.refreshTokenExpires'), payload);

    const {password: userPassword, salt, ...userInfo} = user;

    res.send({
      data: {
        ...userInfo,
        accessToken,
        refreshToken,
      },
      message: 'Login success',
    });
  }

  @Post('registration')
  async registration(
    @Body('email') email,
    @Body('password') password,
    @Body('name') name,
    @Body('surname') surname,
    @Res() res,
  ) {
    if (!email || !password) {
      res.status(400).send({
        error: true,
        message: 'Credentials is invalid',
      });
    }

    const checkUser = await this.userService.getUserByEmail(email);

    if (checkUser) {
      return res.status(400).send({
        error: true,
        message: 'Email already exist',
      });
    }

    const saltRounds = 10;
    const salt = await bcrypt.genSalt(saltRounds);
    const hashedPassword = await bcrypt.hash(password, salt);

    await this.userService.createUser(email, name, surname, hashedPassword, salt);

    res.send({
      message: 'Registration success',
    });
  }

  @Get('redirect/facebook')
  async facebookRedirect(
    // @Query('code') code: string,
  ) {
    // const url = [
    //   `https://graph.facebook.com/v3.3/oauth/access_token?`,
    //   `client_id=${this.config.get('authenticate.facebookAppId')}`,
    //   `&redirect_uri=${this.config.get('authenticate.facebookRedirect')}`,
    //   `&client_secret=${this.config.get('authenticate.facebookAppSecret')}`,
    //   `&code=${code}`,
    // ].join('');
    //
    // const res = await this.http.get(url).toPromise();

    // return res.data;
    return '';
  }

  @Get('refresh-token/:token')
  getRefreshToken(
    @Param('token') token,
    @Res() res,
  ) {
    const secret = this.config.get('application.secret');

    if (!verifyToken(token, secret)) {
      res.status(401).send({
        message: 'You need login again',
      });
    }

    const { iat, exp, ...payload } = jwt.decode(token) as any;
    const accessToken = this.createToken(this.config.get('authenticate.accessTokenExpires'), payload);
    const refreshToken = this.createToken(this.config.get('authenticate.refreshTokenExpires'), payload);

    res.send({
      data: {
        accessToken,
        refreshToken,
      },
    });
  }

  @Post('photo')
  async changeUserPhoto(
    @Query('id') id: number,
    @Req() req: any,
    @Res() res: any,
  ) {
    const user = await this.userService.getById(id);

    const publicFolder = resolve(process.cwd(), 'public');
    const image = req.files.file;
    const ext = getExtension(image.name);
    const defaultImage = '/man.svg';

    if (ext !== 'png' && ext !== 'jpeg' && ext !== 'jpg') {
      return res.status(400).send({error: true, message: 'Bad image'});
    }

    if (user.photo !== defaultImage) {
      const oldImagePath = `${publicFolder}/${user.photo}`;
      unlinkSync(oldImagePath);
    }

    const newAvatar = `${publicFolder}/${image.md5}.${ext}`;

    return image.mv(
      newAvatar,
      async (err) => {
        if (err) {
          return res.status(500).send(err);
        }

        const newImageUrl = `/${image.md5}.${ext}`;
        await this.userService.changePhoto(id, newImageUrl);

        res.json({
          image: newImageUrl,
        });
      },
    );
  }

  createToken(expiresIn: string, payload = {}) {
    return jwt.sign(
      {
        ...payload,
      },
      this.config.get('application.secret'),
      {
        expiresIn: parseInt(expiresIn, 10),
      },
    );
  }
}

const getExtension = (name: string) => {
  const chunks = name.split('.');
  return chunks[chunks.length - 1];
};
