import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {AuthenticateController} from './authenticate.controller';
import { UsersService, UsersModule, UserEntity } from '../users';
import { TripEntity } from '@/modules/trips/entities/trip.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([TripEntity, UserEntity]),
    HttpModule, UsersModule,
  ],
  controllers: [AuthenticateController],
  providers: [UsersService],
})
export class AuthenticateModule {}
