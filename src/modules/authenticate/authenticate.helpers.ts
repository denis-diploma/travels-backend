import * as jwt from 'jsonwebtoken';

export const verifyToken = (token, secret) => {
  try {
    jwt.verify(token, secret);

    return true;
  } catch (e) {
    return false;
  }
};
