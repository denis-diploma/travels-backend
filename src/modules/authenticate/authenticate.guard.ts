import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import {ConfigService} from 'nestjs-config';
import {verifyToken} from './authenticate.helpers';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private readonly config: ConfigService) {}

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    return this.validateRequest(request);
  }

  validateRequest(request) {
    const header = request.headers.authorization;
    const secret = this.config.get('application.secret');

    if (!header) {
      return false;
    }

    return verifyToken(this.getToken(header), secret);
  }

  getToken(header: string) {
    return header.split(' ')[1];
  }
}
