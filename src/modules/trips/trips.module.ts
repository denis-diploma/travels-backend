import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TripsService } from './trips.service';
import { TripEntity } from './entities/trip.entity';
import { TripParticipantsEntity } from './entities/trip-participants.entity';
import { TripPlaceEntity } from './entities/trip-place.entity';
import { TripsResolvers } from './trips.resolvers';
import { UsersModule } from '@/modules/users/users.module';
import { UsersService } from '@/modules/users/users.service';
import { ChatModule } from '@/modules/chat/chat.module';
import { ChatService } from '@/modules/chat/chat.service';
import { UserEntity } from '@/modules/users';
import { ChatEntity } from '@/modules/chat/entities/chat.entity';
import { ChatUserEntity } from '@/modules/chat/entities/chat-user.entity';
import { ChatMessageEntity } from '@/modules/chat/entities/chat-message.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      TripEntity, TripPlaceEntity, TripParticipantsEntity, UserEntity, ChatEntity, ChatUserEntity, ChatMessageEntity,
    ]),
    UsersModule,
    ChatModule,
  ],
  providers: [TripsService, TripsResolvers, ChatService, UsersService],
})
export class TripsModule {
}
