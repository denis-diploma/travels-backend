import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { TripEntity } from './entities/trip.entity';
import { TripParticipantsEntity } from './entities/trip-participants.entity';
import { TripPlaceEntity } from './entities/trip-place.entity';
import { UserEntity } from '../users';
import { ITripFilter, Place } from '@/graphql.schema';
import { ParticipantsStatuses } from '@/modules/trips/participants-statuses';

@Injectable()
export class TripsService {
  constructor(
    @InjectRepository(TripEntity)
    private readonly tripsRepository: Repository<TripEntity>,
    @InjectRepository(TripPlaceEntity)
    private readonly tripPlacesRepository: Repository<TripPlaceEntity>,
    @InjectRepository(TripParticipantsEntity)
    private readonly tripParticipantsRepository: Repository<TripParticipantsEntity>,
  ) {
  }

  async createTrip(
    description: string,
    dateStart: string,
    dateEnd: string,
    local: boolean,
    companion: string,
    withChildren: boolean,
    wishes: string,
    from: Place,
    to: Place,
    author: UserEntity,
    places: TripPlaceEntity[] = [],
    photo: string,
  ): Promise<TripEntity> {
    const savedPlaces = await Promise.all(places.map(place => new TripPlaceEntity(
      place.name,
      place.latitude,
      place.longitude,
      place.order,
    )));

    const f = await this.tripPlacesRepository.save(new TripPlaceEntity(from.name, from.location.lat, from.location.lng));
    const t = await this.tripPlacesRepository.save(new TripPlaceEntity(to.name, to.location.lat, to.location.lng));

    return await this.tripsRepository.save({
      description,
      dateStart,
      dateEnd,
      local,
      companion,
      withChildren,
      wishes,
      from: f,
      to: t,
      author,
      places: savedPlaces,
      photo,
    });
  }

  async delete(tripId: number) {
    const trip = await this.tripsRepository.findOne(tripId, {relations: ['participants']});

    await Promise.all(
      trip.participants.map((item: TripParticipantsEntity) => this.removeParticipant(item.id, tripId)),
    );

    return this.tripsRepository.remove(trip);
  }

  async addParticipant(user: UserEntity, tripId: number) {
    const trip = await this.tripsRepository.findOne(tripId);

    return this.tripParticipantsRepository.save({user, trip, status: ParticipantsStatuses.WAITING});
  }

  async removeParticipant(userId: number, tripId: number): Promise<boolean> {
    const item = await this.tripParticipantsRepository
      .createQueryBuilder('participant')
      .where('participant.user = :user', {user: userId})
      .andWhere('participant.trip = :trip', {trip: tripId})
      .getOne();

    await this.tripParticipantsRepository.remove(item);

    return true;
  }

  async setParticipantStatus(id: number, status: ParticipantsStatuses) {
    const participant = await this.tripParticipantsRepository.findOne({where: {id}});

    participant.status = status;
    await this.tripParticipantsRepository.save(participant);

    return true;
  }

  async getUserTrips(author: number): Promise<TripEntity[]> {
    return this.tripsRepository
      .createQueryBuilder('trip')
      .leftJoinAndSelect('trip.to', 'to')
      .leftJoinAndSelect('trip.from', 'from')
      .leftJoinAndSelect('trip.author', 'author')
      .leftJoinAndSelect('trip.places', 'places')
      .leftJoinAndSelect('trip.participants', 'participants')
      .leftJoinAndSelect('participants.user', 'user')
      .where('trip.author = :author', {author})
      .getMany();
  }

  getParticipantsById(id: number) {
    return this.tripParticipantsRepository.findOne(id, {relations: ['user', 'trip']});
  }

  getParticipantsByTrip(tripId: number, status: string) {
    return this.tripParticipantsRepository
      .createQueryBuilder('participant')
      .leftJoinAndSelect('participant.user', 'user')
      .where('participant.trip = :tripId', {tripId})
      .andWhere('participant.status = :status', {status})
      .getMany();
  }

  async getById(tripId: number): Promise<TripEntity> {
    return this.tripsRepository.createQueryBuilder('trip')
      .leftJoinAndSelect('trip.to', 'to')
      .leftJoinAndSelect('trip.from', 'from')
      .leftJoinAndSelect('trip.author', 'author')
      .leftJoinAndSelect('trip.places', 'places')
      .leftJoinAndSelect('trip.participants', 'participants')
      .leftJoinAndSelect('participants.user', 'user')
      .where('trip.tripId = :tripId', {tripId})
      .getOne();
  }

  async findAll(): Promise<TripEntity[]> {
    return this.tripsRepository
      .createQueryBuilder('trip')
      .leftJoinAndSelect('trip.to', 'to')
      .leftJoinAndSelect('trip.from', 'from')
      .leftJoinAndSelect('trip.author', 'author')
      .leftJoinAndSelect('trip.places', 'places')
      .leftJoinAndSelect('trip.participants', 'participants')
      .leftJoinAndSelect('participants.user', 'user')
      .getMany();
  }

  async find(filter: ITripFilter): Promise<TripEntity[]> {
    const query = await this.tripsRepository
      .createQueryBuilder('trip')
      .leftJoinAndSelect('trip.to', 'to')
      .leftJoinAndSelect('trip.from', 'from')
      .leftJoinAndSelect('trip.author', 'author')
      .leftJoinAndSelect('trip.places', 'places')
      .leftJoinAndSelect('trip.participants', 'participants')
      .leftJoinAndSelect('participants.user', 'user');

    if (filter.dateStart) {
      query.andWhere('trip.dateStart = :dateStart', { dateStart: filter.dateStart });
    }

    if (filter.companion) {
      query.andWhere('trip.companion = :companion', { companion: filter.companion });
    }

    if (filter.withChildren) {
      query.andWhere('trip.withChildren = :withChildren', { withChildren: filter.withChildren });
    }

    if (filter.from && filter.from.location) {
      query.andWhere(getOrthodromeQuery(15, filter.from.location, 'from'));
    }

    if (filter.to && filter.to.location) {
      query.andWhere(getOrthodromeQuery(15, filter.to.location, 'to'));
    }

    return query.getMany();
  }
}

const degreeToRadians = (degree: number) => (Math.PI * degree) / 180;

/*
 Ортодромия - кратчайшая линия между двумя точками на поверхности вращения.
 Угловая длина ортодромии: δ = arccos(sin(f1) * sin(f2) + cos(f1) * cos(f2) * cos(l1 - l2))
 Длина ортодромии: D = δ * R
 f1, l1 - широта и долгота точки отбытия
 f2, l2 - широта и долгота точки прибытия
 δ - угловая длина ортодромии,
 R - радиус земли (6371км)
*/
const getOrthodromeQuery = (radius: number, center: any, column: string) => {
  const earthRadius = 6371;

  const a = Math.sin(degreeToRadians(center.lat));
  const b = Math.cos(degreeToRadians(center.lat));
  const c = degreeToRadians(center.lng);

  return `(${earthRadius} * ACOS(
		      ${a} * SIN(PI() * ${column}.latitude / 180) + 
		      ${b} * COS(PI() * ${column}.latitude / 180) * 
		      COS(${c} - PI() * ${column}.longitude / 180)
	      )) <= ${radius}`;
};
