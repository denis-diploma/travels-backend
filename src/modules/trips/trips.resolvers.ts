import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UsersService } from '@/modules/users';

import { ITripFilter, Place, Trip } from '@/graphql.schema';
import { TripsService } from './trips.service';
import { TripEntity } from './entities/trip.entity';
import { TripParticipantsEntity } from '@/modules/trips/entities/trip-participants.entity';
import { ParticipantsStatuses } from '@/modules/trips/participants-statuses';
import { ChatService } from '@/modules/chat';

@Resolver('Trip')
export class TripsResolvers {
  constructor(
    private readonly tripsService: TripsService,
    private readonly chatService: ChatService,
    private readonly userService: UsersService,
  ) {
  }

  @Mutation('createTrip')
  async createTrip(
    @Args('description') description,
    @Args('dateStart') dateStart,
    @Args('dateEnd') dateEnd,
    @Args('local') local,
    @Args('companion') companion,
    @Args('withChildren') withChildren,
    @Args('wishes') wishes,
    @Args('from') from: Place,
    @Args('to') to: Place,
    @Args('author') authorId: number,
    @Args('places') places,
    @Args('photo') photo,
  ): Promise<TripEntity> {
    if (!authorId) {
      throw new Error('Unknown author');
    }

    const author = await this.userService.getById(authorId);
    const trip = await this.tripsService.createTrip(
      description,
      dateStart,
      dateEnd,
      local,
      companion,
      withChildren,
      wishes,
      from,
      to,
      author,
      places,
      photo,
    );

    await this.chatService.createChat([author], trip);

    return trip;
  }

  @Mutation('addParticipant')
  async addParticipant(
    @Args('userId') userId: number,
    @Args('tripId') tripId: number,
  ): Promise<TripParticipantsEntity> {
    const user = await this.userService.getById(userId);

    return this.tripsService.addParticipant(user, tripId);
  }

  @Mutation('RemoveParticipant')
  async removeParticipant(
    @Args('userId') userId: number,
    @Args('tripId') tripId: number,
  ) {
    return this.tripsService.removeParticipant(userId, tripId);
  }

  @Mutation('SelectParticipant')
  async selectParticipant(
    @Args('id') id: number,
  ) {
    const participant = await this.tripsService.getParticipantsById(id);
    const user = await this.userService.getById(participant.user.userId);
    await this.chatService.addChatMember(participant.trip.tripId, user);

    return this.tripsService.setParticipantStatus(id, ParticipantsStatuses.SELECTED);
  }

  @Mutation('UnselectParticipant')
  async unselectParticipant(
    @Args('id') id: number,
  ) {
    const participant = await this.tripsService.getParticipantsById(id);
    await this.chatService.removeChatMember(participant.trip.tripId, participant.user.userId);

    return this.tripsService.setParticipantStatus(id, ParticipantsStatuses.WAITING);
  }

  @Mutation('DeleteTrip')
  async deleteTrip(
    @Args('tripId') tripId: number,
  ) {
    const chat = await this.chatService.getChatByTrip(tripId);
    await this.chatService.deleteChat(chat.id);

    await this.tripsService.delete(tripId);

    return true;
  }

  @Query('Trips')
  async getTrips(): Promise<TripEntity[]> {
    return this.tripsService.findAll();
  }

  @Query('Trip')
  async getTrip(
    @Args('tripId') tripId: number,
  ) {
    return this.tripsService.getById(tripId);
  }

  @Query('Participants')
  async getParticipants(
    @Args('tripId') tripId: number,
    @Args('status') status: string,
  ) {
    return this.tripsService.getParticipantsByTrip(tripId, status);
  }

  @Query('FilteredTrips')
  async getFilteredTrips(
    @Args('filter') filter: ITripFilter,
  ): Promise<TripEntity[]> {
    return await this.tripsService.find(filter);
  }

  @Query('UserTrips')
  async getUserTrips(
    @Args('id') id: number,
  ) {
    return this.tripsService.getUserTrips(id);
  }
}
