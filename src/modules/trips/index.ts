export {TripsModule} from './trips.module';
export {TripsService} from './trips.service';
export {TripsResolvers} from './trips.resolvers';
export {TripEntity} from './entities/trip.entity';
