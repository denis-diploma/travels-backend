import { Entity, ManyToOne, Column, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';

import { ParticipantsStatuses } from '../participants-statuses';
import { UserEntity } from '@/modules/users/user.entity';
import { TripEntity } from './trip.entity';

@Entity('trips-participants')
export class TripParticipantsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  status: ParticipantsStatuses;

  @ManyToOne(type => UserEntity, user => user.userId)
  @JoinColumn()
  user: UserEntity;

  @ManyToOne(type => TripEntity, trip => trip.tripId)
  @JoinColumn()
  trip: TripEntity;
}
