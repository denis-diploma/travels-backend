import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinTable, OneToMany, OneToOne, JoinColumn, CreateDateColumn } from 'typeorm';
import { UserEntity } from '../../users';
import { TripPlaceEntity } from './trip-place.entity';
import {TripParticipantsEntity} from './trip-participants.entity';

@Entity({ name: 'trips' })
export class TripEntity {
  @PrimaryGeneratedColumn()
  tripId: number;

  @Column('text')
  description: string;

  @Column('date')
  dateStart: Date;

  @Column('date')
  dateEnd: Date;

  @Column('boolean')
  local: boolean;

  @Column('text')
  companion: string;

  @Column('boolean')
  withChildren: boolean;

  @Column('text')
  wishes: string;

  @CreateDateColumn()
  createdAt: string;

  @Column('text')
  photo: string;

  @OneToOne(type => TripPlaceEntity, { cascade: ['insert', 'remove'], onDelete: 'CASCADE' })
  @JoinColumn()
  from: TripPlaceEntity;

  @OneToOne(type => TripPlaceEntity, { cascade: ['insert', 'remove'], onDelete: 'CASCADE' })
  @JoinColumn()
  to: TripPlaceEntity;

  @ManyToOne(type => UserEntity, user => user.userId, { onDelete: 'CASCADE' })
  author: UserEntity;

  @OneToMany(type => TripPlaceEntity, place => place.trip, { cascade: ['insert', 'remove'], onDelete: 'CASCADE' })
  places: TripPlaceEntity[];

  @OneToMany(type => TripParticipantsEntity, participant => participant.trip, {onDelete: 'CASCADE'})
  @JoinTable()
  participants: TripParticipantsEntity[];
}
