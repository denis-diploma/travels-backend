import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import {TripEntity} from './trip.entity';

@Entity({ name: 'trips_places' })
export class TripPlaceEntity {
  constructor(name: string, latitude: number, longitude: number, order: number = 0) {
    this.name = name;
    this.latitude = latitude;
    this.longitude = longitude;
    this.order = order;
  }

  @PrimaryGeneratedColumn()
  tripPlaceId: number;

  @Column('text')
  name: string;

  @Column('decimal', {nullable: true, precision: 11, scale: 8 })
  latitude: number;

  @Column('decimal', {nullable: true, precision: 11, scale: 8})
  longitude: number;

  @Column()
  order: number;

  @ManyToOne(type => TripEntity, trip => trip.places, {onDelete: 'CASCADE'})
  trip: TripEntity;
}
