import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToMany, JoinTable } from 'typeorm';

import { UserGenders } from './user-genders';
import { TripEntity } from '@/modules/trips/entities/trip.entity';

@Entity({ name: 'users' })
export class UserEntity {
  @PrimaryGeneratedColumn()
  userId: number;

  @Column('date', { nullable: true })
  birthday: Date;

  @Column({ nullable: true, type: 'text' })
  gender: UserGenders;

  @Column('text', { nullable: true })
  about: string;

  @Column('text', { nullable: true })
  photo: string;

  @Column('text', { nullable: true })
  country: string;

  @Column('text', { nullable: true })
  city: string;

  @Column('text')
  name: string;

  @Column('text')
  surname: string;

  @Column('text')
  email: string;

  @Column('text', { nullable: true })
  phone: string;

  @Column('text')
  password: string;

  @Column('text')
  salt: string;

  @ManyToMany(() => TripEntity, trip => trip.tripId)
  @JoinTable()
  favorites: TripEntity[];
}
