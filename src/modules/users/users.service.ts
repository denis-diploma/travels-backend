import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UpdateUserInput } from '@/graphql.schema';
import { UserEntity } from './user.entity';
import { TripEntity } from '@/modules/trips/entities/trip.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(TripEntity)
    private readonly tripsRepository: Repository<TripEntity>,
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {
  }

  findAll(): Promise<UserEntity[]> {
    return this.userRepository.find();
  }

  getById(id): Promise<UserEntity> {
    return this.userRepository
      .createQueryBuilder('users')
      .leftJoinAndSelect('users.favorites', 'favorites')
      .leftJoinAndSelect('favorites.to', 'to')
      .leftJoinAndSelect('favorites.from', 'from')
      .leftJoinAndSelect('favorites.author', 'author')
      .leftJoinAndSelect('favorites.participants', 'participants')
      .leftJoinAndSelect('participants.user', 'user')
      .where('users.userId = :id', { id })
      .getOne();
  }

  async changePhoto(id: number, photo: string) {
    const user = await this.userRepository.findOne(id);

    user.photo = photo;

    return this.userRepository.save(user);
  }

  getUserByEmail(email: string): Promise<UserEntity> {
    return this.userRepository.findOne({ email });
  }

  async updateUserInfo(data: UpdateUserInput, userId: number): Promise<UserEntity> {
    const user = await this.userRepository.findOne(userId);

    return this.userRepository.save({ ...user, ...data } as UserEntity);
  }

  async addFavorite(userId: number, tripId: number) {
    const trip = await this.tripsRepository
      .createQueryBuilder('trip')
      .leftJoinAndSelect('trip.to', 'to')
      .leftJoinAndSelect('trip.from', 'from')
      .leftJoinAndSelect('trip.author', 'author')
      .leftJoinAndSelect('trip.places', 'places')
      .leftJoinAndSelect('trip.participants', 'participants')
      .leftJoinAndSelect('participants.user', 'user')
      .where('trip.tripId = :tripId', {tripId})
      .getOne();
    const user = await this.getById(userId);

    user.favorites = [...user.favorites, trip];

    await this.userRepository.save(user);

    return user.favorites;
  }

  async removeFavorite(userId: number, tripId: number) {
    const user = await this.getById(userId);
    user.favorites = user.favorites.filter(item => item.tripId !== tripId);
    await this.userRepository.save(user);
    return user.favorites;
  }

  async createUser(email: string, name: string, surname: string, password: string, salt: string): Promise<UserEntity> {
    const user = this.userRepository.create();

    user.email = email;
    user.name = name;
    user.surname = surname;
    user.password = password;
    user.salt = salt;
    user.photo = '/man.svg';

    await this.userRepository.save(user);

    return this.userRepository.findOne({ email });
  }
}
