import { Args, Query, Mutation, Resolver } from '@nestjs/graphql';

import {User} from '@/graphql.schema';
import {UsersService} from './users.service';
import {UpdateUserInput} from '@/graphql.schema';

@Resolver('User')
export class UsersResolvers {
  constructor(
    private readonly usersService: UsersService,
  ) {}

  @Query('user')
  async getUserByEmail(
    @Args('id') id: number,
  ): Promise<User> {
    return this.usersService.getById(id);
  }

  @Query('UserDetails')
  async getUserDetails(userId: number) {
    return;
  }

  @Query('getUsers')
  async getUsers(): Promise<User[]> {
    return this.usersService.findAll();
  }

  @Query('UserFavoriteTrips')
  async UserFavoriteTrips(
    @Args('userId') userId: number,
  ) {
    const user = await this.usersService.getById(userId);

    return user.favorites;
  }

  @Mutation('updateUserInfo')
  async updateUser(
    @Args('user') user: UpdateUserInput,
    @Args('id') id: number,
  ) {
    return this.usersService.updateUserInfo(user, id);
  }

  @Mutation('addFavoriteTrip')
  async addFavoriteTrip(
    @Args('userId') userId: number,
    @Args('tripId') tripId: number,
  ) {
    return this.usersService.addFavorite(userId, tripId);
  }

  @Mutation('removeFavoriteTrip')
  async removeFavoriteTrip(
    @Args('userId') userId: number,
    @Args('tripId') tripId: number,
  ) {
    return this.usersService.removeFavorite(userId, tripId);
  }
}
