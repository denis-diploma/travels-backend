import { Module } from '@nestjs/common';
import { UsersResolvers } from './users.resolvers';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './user.entity';
import { TripEntity } from '@/modules/trips/entities/trip.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity, TripEntity]),
  ],
  providers: [UsersService, UsersResolvers],
  exports: [UsersService],
})
export class UsersModule {
}
