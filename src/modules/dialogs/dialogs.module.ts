import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { DialogEntity } from './dialog.entity';
import { DialogMessagesEntity } from './dialog-messages.entity';
import { DialogResolvers } from './dialog.resolvers';
import { DialogService } from './dialog.service';
import { UsersService, UsersModule, UserEntity } from '@/modules/users';
import { TripEntity } from '@/modules/trips';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      DialogEntity, DialogMessagesEntity, TripEntity, UserEntity,
    ]),
    UsersModule,
  ],
  providers: [DialogService, DialogResolvers, UsersService],
})
export class DialogsModule {
}
