import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from '@/modules/users';
import {DialogEntity} from './dialog.entity';

@Entity('dialog_messages')
export class DialogMessagesEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserEntity, user => user.userId)
  author: UserEntity;

  @Column('text')
  data: string;

  @ManyToOne(() => DialogEntity, dialog => dialog.id)
  dialog: DialogEntity;

  @Column('datetime')
  timestamp: Date;
}
