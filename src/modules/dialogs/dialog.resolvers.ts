import { Resolver, Query, Mutation, Args, Subscription } from '@nestjs/graphql';
import { PubSub } from 'apollo-server-express';

import { DialogService } from './dialog.service';
import { UsersService } from '@/modules/users';

const pubsub = new PubSub();

const NEW_DIALOG = 'newDialog';
const NEW_DIALOG_MESSAGE = 'newDialogMessage';

@Resolver('Dialog')
export class DialogResolvers {
  constructor(
    private readonly dialogService: DialogService,
    private readonly userService: UsersService,
  ) {
  }

  @Mutation('createDialog')
  async createDialog(
    @Args('creator') creatorId: number,
    @Args('addressee') addresseeId: number,
  ) {
    const creator = await this.userService.getById(creatorId);
    const addressee = await this.userService.getById(addresseeId);

    const dialog = await this.dialogService.createDialog(creator, addressee);

    pubsub.publish(NEW_DIALOG, { [NEW_DIALOG]: dialog });

    return dialog;
  }

  @Mutation('sendDialogMessage')
  async sendDialogMessage(
    @Args('author') authorId: number,
    @Args('dialog') dialogId: number,
    @Args('message') message: string,
  ) {
    const author = await this.userService.getById(authorId);
    const dialog = await this.dialogService.getById(dialogId);

    const newMessage = await this.dialogService.createMessage(author, dialog, message);

    pubsub.publish(NEW_DIALOG_MESSAGE, {[NEW_DIALOG_MESSAGE]: newMessage});

    return newMessage;
  }

  @Query('Dialogs')
  getAllDialogs() {
    return this.dialogService.getAll();
  }

  @Query('Dialog')
  getDialog(
    @Args('id') id: number,
  ) {
    return this.dialogService.getById(id);
  }

  @Query('UserDialogs')
  getUserDialogs(
    @Args('id') id: number,
  ) {
    return this.dialogService.getByUser(id);
  }

  @Query('DialogMessages')
  getMessagesByDialog(
    @Args('dialog') dialogId: number,
  ) {
    return this.dialogService.getMessagesByDialog(dialogId);
  }

  @Subscription(NEW_DIALOG)
  async onNewDialog() {
    return pubsub.asyncIterator(NEW_DIALOG);
  }

  @Subscription(NEW_DIALOG_MESSAGE)
  async onNewDialogMessage() {
    return pubsub.asyncIterator(NEW_DIALOG_MESSAGE);
  }
}
