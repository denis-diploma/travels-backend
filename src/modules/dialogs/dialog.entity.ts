import { Entity, ManyToOne, PrimaryGeneratedColumn, Index } from 'typeorm';

import { UserEntity } from '@/modules/users';

@Entity('dialogs')
@Index(['creator', 'addressee'], {unique: true})
export class DialogEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserEntity, user => user.userId, {cascade: true})
  creator: UserEntity;

  @ManyToOne(() => UserEntity, user => user.userId, {cascade: true})
  addressee: UserEntity;
}
