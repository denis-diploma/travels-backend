import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { DialogEntity } from './dialog.entity';
import { DialogMessagesEntity } from './dialog-messages.entity';
import { UserEntity } from '@/modules/users';

@Injectable()
export class DialogService {
  constructor(
    @InjectRepository(DialogEntity)
    private readonly dialogRepository: Repository<DialogEntity>,
    @InjectRepository(DialogMessagesEntity)
    private readonly dialogMessagesRepository: Repository<DialogMessagesEntity>,
  ) {
  }

  async createDialog(creator: UserEntity, addressee: UserEntity) {
    const dialog = await this.dialogRepository.create({creator, addressee});

    return this.dialogRepository.save(dialog);
  }

  async createMessage(author: UserEntity, dialog: DialogEntity, data: string) {
    return await this.dialogMessagesRepository.save({
      author,
      dialog,
      data,
      timestamp: new Date().toISOString(),
    });
  }

  async getById(id: number) {
    return this.dialogRepository.findOne(id, {
      relations: ['creator', 'addressee'],
    });
  }

  async getByUser(id: number) {
    return this.dialogRepository
      .createQueryBuilder('dialog')
      .leftJoinAndSelect('dialog.creator', 'creator')
      .leftJoinAndSelect('dialog.addressee', 'addressee')
      .where('dialog.creator = :id', {id})
      .orWhere('dialog.addressee = :id', {id})
      .getMany();
  }

  async getMessagesByDialog(dialog: number) {
    return this.dialogMessagesRepository.find({
      where: {dialog},
      relations: ['author', 'dialog'],
    });
  }

  getAll() {
    return this.dialogRepository.find({
      relations: ['creator', 'addressee'],
    });
  }
}
