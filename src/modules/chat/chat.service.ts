import {Injectable} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {ChatEntity} from './entities/chat.entity';
import {ChatUserEntity} from './entities/chat-user.entity';
import {ChatMessageEntity} from './entities/chat-message.entity';
import {UserEntity} from '@/modules/users';
import {TripEntity} from '@/modules/trips';

@Injectable()
export class ChatService {
  constructor(
    @InjectRepository(ChatEntity)
    private readonly chatRepository: Repository<ChatEntity>,
    @InjectRepository(ChatUserEntity)
    private readonly chatUserRepository: Repository<ChatUserEntity>,
    @InjectRepository(ChatMessageEntity)
    private readonly messageRepository: Repository<ChatMessageEntity>,
  ) {}

  async createChat(users: UserEntity[], trip: TripEntity): Promise<ChatEntity> {
    const chat = new ChatEntity();

    chat.users = users;
    chat.trip = trip;

    return await this.chatRepository.save(chat);
  }

  async createMessage(author: UserEntity, chat: ChatEntity, data: string) {
    const {id} = await this.messageRepository.save({
      author,
      data,
      chat,
      timestamp: new Date().toISOString(),
    });

    return this.messageRepository
      .createQueryBuilder('message')
      .leftJoinAndSelect('message.author', 'author')
      .leftJoinAndSelect('message.chat', 'chat')
      .where('message.id = :id', {id})
      .getOne();
  }

  async getUserChats(userId) {
    return this.chatRepository
      .createQueryBuilder('chat')
      .leftJoinAndSelect('chat.trip', 'trip')
      .leftJoinAndSelect('trip.to', 'to')
      .leftJoinAndSelect('trip.from', 'from')
      .leftJoinAndSelect('trip.author', 'author')
      .leftJoinAndSelect('chat.users', 'users')
      .where(qb => {
        const query = qb
          .subQuery()
          .select('inner_chat.id')
          .from(ChatEntity, 'inner_chat')
          .innerJoin('inner_chat.users', 'users', 'users.userId = :userId')
          .getQuery();

        return 'chat.id IN ' + query;
      })
      .setParameter('userId', userId)
      .getMany();
  }

  async getMessagesByChat(chatId: number) {
    return this.messageRepository
      .createQueryBuilder('message')
      .leftJoinAndSelect('message.author', 'author')
      .leftJoinAndSelect('message.chat', 'chat')
      .where('message.chat = :chatId', {chatId})
      .orderBy('message.timestamp', 'ASC')
      .getMany();
  }

  async deleteChat(chatId) {
    const chat = await this.getById(chatId);

    return this.chatRepository.remove(chat);
  }

  async getAllChats() {
    return this.chatRepository
      .createQueryBuilder('chat')
      .leftJoinAndSelect('chat.users', 'users')
      .leftJoinAndSelect('chat.trip', 'trip')
      .getMany();
  }

  async getChatByTrip(tripId: number) {
    return await this.chatRepository
      .createQueryBuilder('chat')
      .leftJoinAndSelect('chat.users', 'users')
      .innerJoin('chat.trip', 'trip', 'trip.tripId = :tripId')
      .setParameter('tripId', tripId)
      .getOne();
  }

  async addChatMember(tripId: number, user: UserEntity) {
    const chat = await this.getChatByTrip(tripId);

    chat.users = [...chat.users, user];

    return this.chatRepository.save(chat);
  }

  async removeChatMember(tripId: number, userId: number) {
    const chat = await this.getChatByTrip(tripId);

    chat.users = chat.users.filter((item: UserEntity) => item.userId !== userId);

    return this.chatRepository.save(chat);
  }

  async getById(chatId: number) {
    return this.chatRepository
      .createQueryBuilder('chat')
      .leftJoinAndSelect('chat.trip', 'trip')
      .leftJoinAndSelect('trip.to', 'to')
      .leftJoinAndSelect('trip.from', 'from')
      .leftJoinAndSelect('trip.author', 'author')
      .leftJoinAndSelect('chat.users', 'users')
      .where('chat.id = :chatId', {chatId})
      .getOne();
  }
}
