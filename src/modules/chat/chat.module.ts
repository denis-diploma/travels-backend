import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ChatEntity } from './entities/chat.entity';
import { ChatUserEntity } from './entities/chat-user.entity';
import { ChatMessageEntity } from './entities/chat-message.entity';
import { ChatService } from './chat.service';
import { ChatResolvers } from './chat.resolvers';
import { UsersService, UsersModule, UserEntity } from '@/modules/users';
import { TripEntity } from '@/modules/trips';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ChatEntity, ChatUserEntity, ChatMessageEntity, TripEntity, UserEntity,
    ]),
    UsersModule,
  ],
  providers: [ChatService, ChatResolvers, UsersService],
})
export class ChatModule {
}
