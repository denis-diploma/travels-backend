import { Args, Subscription, Mutation, Query, Resolver } from '@nestjs/graphql';
import {PubSub} from 'apollo-server-express';

import { UsersService } from '@/modules/users/users.service';
import { ChatService } from './chat.service';

const pubsub = new PubSub();
const NEW_CHAT_SUBSCRIPTION_KEY = 'newChat';
const NEW_MESSAGE_SUBSCRIPTION_KEY = 'newChatMessage';

@Resolver('Chat')
export class ChatResolvers {
  constructor(
    private readonly chatService: ChatService,
    private readonly userService: UsersService,
  ) {
  }

  @Mutation('deleteChat')
  async deleteChat(
    @Args('chat') chatId: number,
  ) {

    await this.chatService.deleteChat(chatId);

    return chatId;
  }

  @Query('Chats')
  async getChats() {
    return this.chatService.getAllChats();
  }

  @Query('Chat')
  async getChat(
    @Args('chatId') chatId: number,
  ) {
    return this.chatService.getById(chatId);
  }

  @Query('UserChats')
  async getUserChats(
    @Args('user') userId: number,
  ) {
    return this.chatService.getUserChats(userId);
  }

  @Query('ChatsMessages')
  async getMessages(
    @Args('chat') chatId: number,
  ) {
    return this.chatService.getMessagesByChat(chatId);
  }

  @Mutation('sendChatMessage')
  async createMessage(
    @Args('author') authorId: number,
    @Args('chat') chatId: number,
    @Args('message') message: string,
  ) {
    const author = await this.userService.getById(authorId);
    const chat = await this.chatService.getById(chatId);
    const newMessage = await this.chatService.createMessage(author, chat, message);

    pubsub.publish(NEW_MESSAGE_SUBSCRIPTION_KEY, {[NEW_MESSAGE_SUBSCRIPTION_KEY]: newMessage});

    return newMessage;
  }

  @Subscription(NEW_MESSAGE_SUBSCRIPTION_KEY)
  async onNewMessage(@Args('chatsIds') chatsIds: number[]) {
    return pubsub.asyncIterator(NEW_MESSAGE_SUBSCRIPTION_KEY);
  }

  @Subscription(NEW_CHAT_SUBSCRIPTION_KEY)
  async onNewChat(@Args('userId') userId: number) {
    return pubsub.asyncIterator(NEW_CHAT_SUBSCRIPTION_KEY);
  }
}
