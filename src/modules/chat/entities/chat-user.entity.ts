import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { ChatEntity } from './chat.entity';
import { UserEntity } from '@/modules/users';

@Entity('chats_users')
export class ChatUserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => ChatEntity, chat => chat.users, {onDelete: 'CASCADE'})
  @JoinColumn()
  chat: ChatEntity;

  @ManyToOne(type => UserEntity, user => user.userId)
  @JoinColumn()
  user: UserEntity;
}
