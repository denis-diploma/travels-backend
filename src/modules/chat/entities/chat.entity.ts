import { Entity, JoinColumn, PrimaryGeneratedColumn, OneToOne, ManyToMany, JoinTable } from 'typeorm';

import { UserEntity } from '@/modules/users';
import { TripEntity } from '@/modules/trips';

@Entity('chats')
export class ChatEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => UserEntity, {onDelete: 'NO ACTION'})
  @JoinTable()
  users: UserEntity[];

  @OneToOne(() => TripEntity, trip => trip.tripId, {onDelete: 'NO ACTION'})
  @JoinColumn()
  trip: TripEntity;
}
