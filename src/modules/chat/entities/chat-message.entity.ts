import { Entity, Column, ManyToOne, JoinColumn, PrimaryGeneratedColumn } from 'typeorm';

import { ChatEntity } from './chat.entity';
import { UserEntity } from '@/modules/users';

@Entity('chat_messages')
export class ChatMessageEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => UserEntity, user => user.userId)
  @JoinColumn()
  author: UserEntity;

  @Column('text')
  data: string;

  @ManyToOne(() => ChatEntity, chat => chat.id, {onDelete: 'CASCADE'})
  @JoinColumn()
  chat: ChatEntity;

  @Column('datetime')
  timestamp: Date;
}
