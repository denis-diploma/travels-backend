import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from 'nestjs-config';
import { GraphQLModule } from '@nestjs/graphql';
import * as path from 'path';

import { AuthenticateModule } from '../authenticate';
import { UsersModule } from '@/modules/users/users.module';
import { TripsModule } from '@/modules/trips/trips.module';
import { ChatModule } from '@/modules/chat/chat.module';
import { DialogsModule } from '@/modules/dialogs/dialogs.module';

@Module({
    imports: [
    AuthenticateModule,
    UsersModule,
    TripsModule,
    ChatModule,
    DialogsModule,

    ConfigModule.load(path.resolve(__dirname, '../../config', '**', '!(*.d).{js,ts}')),
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get('database'),
      inject: [ConfigService],
    }),
    GraphQLModule.forRoot({
      debug: true,
      playground: true,
      installSubscriptionHandlers: true,
      typePaths: [
        path.resolve(process.cwd(), 'src/**/*.graphql'),
      ],
    }),
  ],
})
export class AppModule {
}
