export default {
  debug: process.env.GRAPHQL_DEBUG === 'true',
  playground: process.env.GRAPHQL_PLAYGROUND === 'true',
};
