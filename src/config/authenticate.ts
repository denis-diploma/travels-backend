export default {
  accessTokenExpires: process.env.ACCESS_TOKEN_EXPIRES || 3600,
  refreshTokenExpires: process.env.REFRESH_TOKEN_EXPIRES || 36000,
  facebookAppId: process.env.FACEBOOK_APP_ID || '',
  facebookAppSecret: process.env.FACEBOOK_APP_SECRET || '',
  facebookRedirect: process.env.FACEBOOK_REDIRECT || '',
};
