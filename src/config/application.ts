export default {
  dev: process.env.NODE_ENV === 'development',
  port: process.env.PORT || 3000,
  secret: process.env.SECRET || 'secret',
  staticHost: process.env.STATIC_HOST || 'http://localhost:3000',
};
