import {SnakeNamingStrategy} from '@/libs/typeorm-snakecase-naming';
import appConfig from './application';

const folder = appConfig.dev ? 'src' : 'dist';
const extension = appConfig.dev ? 'ts' : 'js';

export default {
  type: process.env.TYPEORM_TYPE || 'mysql',
  host: process.env.TYPEORM_HOST || 'localhost',
  port: process.env.TYPEORM_PORT || 3306,
  username: process.env.TYPEORM_USERNAME || 'root',
  password: process.env.TYPEORM_PASSWORD || 'password',
  database: process.env.TYPEORM_DATABASE,
  logging: process.env.TYPEORM_LOGGING === 'true',
  synchronize: process.env.TYPEORM_SYNCHRONIZE === 'true',
  entities: [
    `${folder}/**/*.entity.${extension}`,
  ],
  cli: {
    migrationsDir: 'src/migrations',
  },
  namingStrategy: new SnakeNamingStrategy(),
};
