
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
export class ITripFilter {
    dateStart?: string;
    companion?: string;
    from?: Place;
    to?: Place;
    withChildren?: boolean;
}

export class Location {
    lat?: number;
    lng?: number;
}

export class Place {
    location?: Location;
    name?: string;
}

export class TripPlaceInput {
    name?: string;
    latitude?: string;
    longitude?: string;
    order?: number;
}

export class UpdateUserInput {
    birthday?: Date;
    gender?: string;
    about?: string;
    name?: string;
    surname?: string;
    email?: string;
    phone?: string;
}

export class Chat {
    id?: number;
    messages?: Message[];
    users?: ChatUser[];
}

export class ChatUser {
    id?: number;
    chat?: Chat;
    user?: User;
}

export class Message {
    id?: number;
    data?: string;
    timestamp?: string;
    author?: ChatUser;
}

export abstract class IMutation {
    abstract createDialog(firstUser?: number, secondUser?: number): Chat | Promise<Chat>;

    abstract deleteChat(chat?: number): number | Promise<number>;

    abstract sendMessage(author?: number, message?: string): Message | Promise<Message>;

    abstract createTrip(description?: string, dateStart?: string, dateEnd?: string, local?: boolean, companion?: string, withChildren?: boolean, wishes?: string, from?: Place, to?: Place, author?: number, places?: TripPlaceInput[]): Trip | Promise<Trip>;

    abstract addParticipant(userId?: number, tripId?: number): Participant | Promise<Participant>;

    abstract createUser(email?: string, password?: string): User | Promise<User>;

    abstract updateUserInfo(user?: UpdateUserInput, id?: number): User | Promise<User>;
}

export class Participant {
    user?: User;
    status?: string;
}

export abstract class IQuery {
    abstract Chat(chatId?: number): Chat | Promise<Chat>;

    abstract Chats(): Chat[] | Promise<Chat[]>;

    abstract Messages(chat?: number): Message[] | Promise<Message[]>;

    abstract UserChats(user?: number): Chat[] | Promise<Chat[]>;

    abstract Trips(): Trip[] | Promise<Trip[]>;

    abstract Trip(tripId?: number): Trip | Promise<Trip>;

    abstract FilteredTrips(filter?: ITripFilter): Trip[] | Promise<Trip[]>;

    abstract UserTrips(id?: number): Trip[] | Promise<Trip[]>;

    abstract Participants(tripId?: number, status?: string): Participant[] | Promise<Participant[]>;

    abstract getUsers(): User[] | Promise<User[]>;

    abstract user(id?: number): User | Promise<User>;

    abstract temp__(): boolean | Promise<boolean>;
}

export abstract class ISubscription {
    abstract newMessage(chatsIds?: number[]): Message | Promise<Message>;

    abstract newChat(userId?: number): Chat | Promise<Chat>;
}

export class Trip {
    tripId?: number;
    description?: string;
    dateStart?: string;
    dateEnd?: string;
    local?: boolean;
    companion?: string;
    withChildren?: boolean;
    wishes?: string;
    from?: TripPlace;
    to?: TripPlace;
    author?: User;
    places?: TripPlace;
    participants?: User[];
}

export class TripPlace {
    tripPlaceId?: number;
    name?: string;
    latitude?: number;
    longitude?: number;
    order?: number;
}

export class User {
    userId?: number;
    birthday?: Date;
    gender?: string;
    about?: string;
    photo?: string;
    name?: string;
    surname?: string;
    country?: string;
    city?: string;
    phone?: string;
    email?: string;
}

export type Date = any;
