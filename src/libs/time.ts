export const millisecondsToSeconds = (time: number) => Math.floor(time / 1000);
