import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app';
import * as fileUpload from 'express-fileupload';
import * as express from 'express';
import { resolve } from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app
    .enableCors({
      origin: '*',
    })
    .use('/static', express.static(resolve(process.cwd(), 'public')))
    .use(fileUpload({
      limits: { fileSize: 50 * 1024 * 1024 },
    }))
    .setGlobalPrefix('travels');
  await app.listen(process.env.PORT);
  console.log(`Backend starting in http://localhost:${process.env.PORT}/`);
}

bootstrap();
