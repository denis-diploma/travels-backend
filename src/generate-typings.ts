import { GraphQLDefinitionsFactory } from '@nestjs/graphql';
import { join } from 'path';

const dev = process.env.NODE_ENV === 'development';
const path = dev ? 'src/graphql.schema.ts' : 'dist/graphql.schema.ts';

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
  typePaths: ['./src/**/*.graphql'],
  path: join(process.cwd(), path),
  outputAs: 'class',
  watch: true,
});
