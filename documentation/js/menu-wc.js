'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">backend documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                        <li class="link">
                            <a href="dependencies.html" data-type="chapter-link">
                                <span class="icon ion-ios-list"></span>Dependencies
                            </a>
                        </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse" ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthenticateModule.html" data-type="entity-link">AuthenticateModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' : 'data-target="#xs-controllers-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' :
                                            'id="xs-controllers-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' }>
                                            <li class="link">
                                                <a href="controllers/AuthenticateController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthenticateController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' : 'data-target="#xs-injectables-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' :
                                        'id="xs-injectables-links-module-AuthenticateModule-b0bd3bac4cf2f69c524d3bce23d84f2a"' }>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/ChatModule.html" data-type="entity-link">ChatModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ChatModule-8242047732572db6150cdf9a401a0a40"' : 'data-target="#xs-injectables-links-module-ChatModule-8242047732572db6150cdf9a401a0a40"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ChatModule-8242047732572db6150cdf9a401a0a40"' :
                                        'id="xs-injectables-links-module-ChatModule-8242047732572db6150cdf9a401a0a40"' }>
                                        <li class="link">
                                            <a href="injectables/ChatService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ChatService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/DialogsModule.html" data-type="entity-link">DialogsModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-DialogsModule-ba9fa615c9fa7e767b6c76b9af95e4ff"' : 'data-target="#xs-injectables-links-module-DialogsModule-ba9fa615c9fa7e767b6c76b9af95e4ff"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-DialogsModule-ba9fa615c9fa7e767b6c76b9af95e4ff"' :
                                        'id="xs-injectables-links-module-DialogsModule-ba9fa615c9fa7e767b6c76b9af95e4ff"' }>
                                        <li class="link">
                                            <a href="injectables/DialogService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DialogService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/TripsModule.html" data-type="entity-link">TripsModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-TripsModule-2ed0a589367a6dadf9f4e5e343384c0b"' : 'data-target="#xs-injectables-links-module-TripsModule-2ed0a589367a6dadf9f4e5e343384c0b"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-TripsModule-2ed0a589367a6dadf9f4e5e343384c0b"' :
                                        'id="xs-injectables-links-module-TripsModule-2ed0a589367a6dadf9f4e5e343384c0b"' }>
                                        <li class="link">
                                            <a href="injectables/ChatService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ChatService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/TripsService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TripsService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-e8b8062860a3a9ffb2fca93c03d82c45"' : 'data-target="#xs-injectables-links-module-UsersModule-e8b8062860a3a9ffb2fca93c03d82c45"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-e8b8062860a3a9ffb2fca93c03d82c45"' :
                                        'id="xs-injectables-links-module-UsersModule-e8b8062860a3a9ffb2fca93c03d82c45"' }>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#controllers-links"' :
                                'data-target="#xs-controllers-links"' }>
                                <span class="icon ion-md-swap"></span>
                                <span>Controllers</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links"' : 'id="xs-controllers-links"' }>
                                <li class="link">
                                    <a href="controllers/AppController.html" data-type="entity-link">AppController</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Chat.html" data-type="entity-link">Chat</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChatEntity.html" data-type="entity-link">ChatEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChatMessageEntity.html" data-type="entity-link">ChatMessageEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChatResolvers.html" data-type="entity-link">ChatResolvers</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChatUser.html" data-type="entity-link">ChatUser</a>
                            </li>
                            <li class="link">
                                <a href="classes/ChatUserEntity.html" data-type="entity-link">ChatUserEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/DialogEntity.html" data-type="entity-link">DialogEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/DialogMessagesEntity.html" data-type="entity-link">DialogMessagesEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/DialogResolvers.html" data-type="entity-link">DialogResolvers</a>
                            </li>
                            <li class="link">
                                <a href="classes/IMutation.html" data-type="entity-link">IMutation</a>
                            </li>
                            <li class="link">
                                <a href="classes/IQuery.html" data-type="entity-link">IQuery</a>
                            </li>
                            <li class="link">
                                <a href="classes/ISubscription.html" data-type="entity-link">ISubscription</a>
                            </li>
                            <li class="link">
                                <a href="classes/ITripFilter.html" data-type="entity-link">ITripFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/Location.html" data-type="entity-link">Location</a>
                            </li>
                            <li class="link">
                                <a href="classes/Message.html" data-type="entity-link">Message</a>
                            </li>
                            <li class="link">
                                <a href="classes/Participant.html" data-type="entity-link">Participant</a>
                            </li>
                            <li class="link">
                                <a href="classes/Place.html" data-type="entity-link">Place</a>
                            </li>
                            <li class="link">
                                <a href="classes/SnakeNamingStrategy.html" data-type="entity-link">SnakeNamingStrategy</a>
                            </li>
                            <li class="link">
                                <a href="classes/Trip.html" data-type="entity-link">Trip</a>
                            </li>
                            <li class="link">
                                <a href="classes/TripEntity.html" data-type="entity-link">TripEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/TripParticipantsEntity.html" data-type="entity-link">TripParticipantsEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/TripPlace.html" data-type="entity-link">TripPlace</a>
                            </li>
                            <li class="link">
                                <a href="classes/TripPlaceEntity.html" data-type="entity-link">TripPlaceEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/TripPlaceInput.html" data-type="entity-link">TripPlaceInput</a>
                            </li>
                            <li class="link">
                                <a href="classes/TripsResolvers.html" data-type="entity-link">TripsResolvers</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateUserInput.html" data-type="entity-link">UpdateUserInput</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserEntity.html" data-type="entity-link">UserEntity</a>
                            </li>
                            <li class="link">
                                <a href="classes/UsersResolvers.html" data-type="entity-link">UsersResolvers</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AppService.html" data-type="entity-link">AppService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse" ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});