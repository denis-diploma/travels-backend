// const tsConfig = require('./tsconfig.build.json');
const tsConfigPaths = require('tsconfig-paths');

const baseUrl = './';
tsConfigPaths.register({
  baseUrl,
  paths: {
    "@/*": [
      "dist/*"
    ]
  },
});
