FROM node:10-alpine

WORKDIR /opt/app

RUN apk add --no-cache --virtual .gyp \
        python \
        make \
        g++

ADD package*.json .
ADD yarn.lock .

RUN yarn

COPY . .
RUN yarn build

RUN apk del .gyp

CMD ["yarn", "start:prod"]
